import eel
import sys

from libs.TablesManagers import TableManager
from libs.resource_path import resource_path
from libs.filling_tables import filling_tables
from libs.init_exposes import init_exposes

eel = init_exposes(eel)
filling_tables()

chrome_path = resource_path(
    main_file=__file__,
    relative_path='./assets/GoogleChromePortable/GoogleChromePortable.exe'
)

if __name__ == "__main__":
    eel.init('assets')
    eel.start(
        'index.html',
        mode='my_portable_chrome',
        port=8000,
        chrome_path=chrome_path,
    )
